import os
import sys
import json
import copy
import subprocess
import traceback
import datetime
from collections import OrderedDict
import sublime, sublime_plugin


# ------------------------------------------------------------------ #
#   Ensure we keep a copy of the default environment                 #
# ------------------------------------------------------------------ #

DEFAULT_ENV = OrderedDict()

# this is fired only once when the plugin gets loaded
def plugin_loaded():
    global DEFAULT_ENV
    DEFAULT_ENV.clear()
    DEFAULT_ENV.update(os.environ)

def to_default_environment():
    os.environ.clear()
    os.environ.update(DEFAULT_ENV)

# ------------------------------------------------------------------ #

def combine_dicts(dicta, dictb):
    if isinstance(dicta, OrderedDict):
        updated = copy.deepcopy(dicta)
    else:
        updated = OrderedDict()
        for key, value in dicta.items():
            updated[key] = copy.deepcopy(value)

    for k2, v2 in dictb.items():
        updated[k2] = copy.deepcopy(v2)
    return updated


def pretty_print_dict(d, out_callback=sys.stdout.write):
    max_key_length = 0
    keys = []
    for key in d.keys():
        keys.append(key)
        len_key = len(key)
        if len_key > max_key_length:
            max_key_length = len_key

    keys.sort()

    log_format = '{:>' + str(max_key_length) + '} = {}'
    indent_prefix = '\n' + ' ' * (max_key_length + 3)

    for key in keys:
        value = d[key]
        if 'path' in key.lower():
            value = [v for v in value.split(os.pathsep) if v]
            value = indent_prefix.join(value)

        out_callback(log_format.format(key, value))


def print_as_title(title, decoration_char, upper_decoration=False, out_callback=sys.stdout.write):
    if upper_decoration:
        out_callback(decoration_char * len(title))
    out_callback(title)
    out_callback(decoration_char * len(title))


def get_combined_settings():
    plugin_settings = sublime.load_settings("ProjectEnvironment.sublime-settings")
    plugin_settings_dic = OrderedDict()
    for key in ('print_output', 'set_sublime_variables', 'sublime_variables_prefix', 'sublime_variables_capitalized',
                'command_line_to_wrap_extensions', 'execute_ext_with'):
        plugin_settings_dic[key] = plugin_settings.get(key)

    proj_env_setts = get_project_env_settings()
    if proj_env_setts:
        return combine_dicts(plugin_settings_dic, proj_env_setts)
    else:
        return plugin_settings


def get_active_project_name():
    window_vars = sublime.active_window().extract_variables()
    return window_vars.get('project_base_name', None)


def get_project_env_settings():
    proj_data = sublime.active_window().project_data()
    if not proj_data:
        return OrderedDict()
    try:
        proj_env_setts = proj_data['settings']['project_environment']
        return proj_env_setts
    except KeyError:
        return OrderedDict()



class ProjectEnvironmentLog:
    def __init__(self, window):
        self.window = window
        self.panel = window.find_output_panel('project_environment_log')
        if not self.panel:
            self.panel = window.create_output_panel('project_environment_log')

        settings = self.panel.settings()
        settings.set('auto_indent', False)
        settings.set('smart_indent', False)
        settings.set('indent_to_bracket', False)
        settings.set('indent_subsequent_lines', False)
        settings.set('trim_automatic_white_space', False)
        settings.set('copy_with_empty_selection', True)

        self.mute = False

    def __call__(self, *msgs):
        if self.mute:
            return

        full_msg = ' '.join([str(m) for m in msgs]) + '\n'
        self.panel.run_command('insert', {'characters': full_msg})

    def clear(self):
        self.panel.run_command('select_all')
        self.panel.run_command('insert', {'characters': ''})

    def show(self):
        self.window.run_command('show_panel', {'panel': 'output.project_environment_log'})

    def hide(self):
        self.window.run_command('hide_panel', {'panel': 'output.project_environment_log'})


class ProjectEnvironmentListener(sublime_plugin.EventListener):
    def __init__(self, *args, **kwds):
        super(ProjectEnvironmentListener, self).__init__(*args, **kwds)

        self.active_window  = None
        self.active_project = None

    def on_activated_async(self, view):
        if self.active_window == sublime.active_window():
            if self.active_project == sublime.active_window().project_file_name():
                return
        self.active_window  = sublime.active_window()
        self.active_project = sublime.active_window().project_file_name()

        sublime.active_window().run_command('set_project_environment')
        sublime.active_window().run_command('update_project_data')

    def on_post_save(self, view):
        if view.file_name() == sublime.active_window().project_file_name():
            sublime.active_window().run_command('set_project_environment')
            sublime.active_window().run_command('update_project_data')


class SetProjectEnvironmentCommand(sublime_plugin.WindowCommand):
    def __init__(self, window):
        super().__init__(window)
        self.log = ProjectEnvironmentLog(window)

    def is_enabled(self):
        if get_project_env_settings():
            return True
        else:
            return False

    def run(self):
        new_envs = OrderedDict()
        new_envs['sublime'] = OrderedDict()
        new_envs['inline_env'] = OrderedDict()
        new_envs['command_file_env'] = OrderedDict()

        to_default_environment()
        settings = get_combined_settings()
        platform = sublime.platform()
        window_vars = self.window.extract_variables()
        platform_settings = settings.get(platform, OrderedDict())

        if settings.get('print_output'):
            self.log.mute = False
        else:
            self.log.mute = True

        self.log('\n')
        time_ = datetime.datetime.now().strftime("%H:%M:%S")
        t = 'Updating {}\'s Environment at {}'.format(get_active_project_name(), time_)
        print_as_title(t, '=', True, self.log)
        self.log()

        # ------------------------------------------------------------------ #
        #   Collect variables from Sublime itself                            #
        # ------------------------------------------------------------------ #

        if settings.get('set_sublime_variables'):
            keys = ("project_path", "project", "project_name", "project_base_name", "packages")
            prefix = settings.get('sublime_variables_prefix', '')
            capit = settings.get('sublime_variables_capitalized', False)
            for key in keys:
                env_key = prefix + key
                if capit:
                    env_key = env_key.upper()
                new_envs['sublime'][env_key] = window_vars[key]

            print_as_title('From Sublime', '-', False, self.log)  # new_envs['sublime'])
            pretty_print_dict(new_envs['sublime'], self.log)
            self.log()

            for key, value in new_envs['sublime'].items():
                os.environ[key] = os.path.expandvars(value)

        # ------------------------------------------------------------------ #
        #   Collect the variables from an external file. This can be a json  #
        #   file or a shell or batch file or even an executable              #
        # ------------------------------------------------------------------ #

        main_file_env = self.process_env_file(settings.get('env_file'), settings, window_vars)
        platform_file_env = self.process_env_file(platform_settings.get('env_file'), settings, window_vars)
        combined_file_env = combine_dicts(main_file_env, platform_file_env)

        if combined_file_env:
            new_envs['command_file_env'] = combined_file_env
            for key, value in new_envs['command_file_env'].items():
                os.environ[key] = os.path.expandvars(value)

        # ------------------------------------------------------------------ #
        #   Collect the "inline" variables                                   #
        # ------------------------------------------------------------------ #

        main_inline_env     = settings.get('env', OrderedDict())
        platform_inline_env = platform_settings.get('env', OrderedDict())
        combined_inline_env = combine_dicts(main_inline_env, platform_inline_env)

        if combined_inline_env:
            new_envs['inline_env'] = combined_inline_env
            print_as_title('Inline Variables', '-', False, self.log)
            pretty_print_dict(new_envs['inline_env'], self.log)
            self.log()
            for key, value in new_envs['inline_env'].items():
                os.environ[key] = os.path.expandvars(value)

        # ------------------------------------------------------------------ #
        #   Group all the variables together and set up the new environment  #
        # ------------------------------------------------------------------ #

        all_new_vars = list(new_envs['sublime'].keys()) + list(new_envs['command_file_env'].keys()) + list(new_envs['inline_env'].keys())
        print_as_title('Complete Project Environment', '-', False, self.log)
        pretty_print_dict({key: os.environ.get(key) for key in all_new_vars}, self.log)
        self.log()


    def process_env_file(self, env_file, settings, window_vars):
        file_vars = {}

        if env_file:
            env_file = os.path.expandvars(env_file)
            if env_file.startswith('.'):
                env_file = os.path.abspath(os.path.join(window_vars['project_path'], env_file))

            _, ext = os.path.splitext(env_file)
            if ext.lower() == '.json':
                # collect variables from a json file
                file_vars, err = self.collect_variables_from_json_file(env_file, window_vars['project_path'])
            elif ext in settings.get('command_line_to_wrap_extensions'):
                file_vars, err = self.collect_variables_from_command_line_file(env_file)
            else:
                # assume it is a command line executable file
                program = settings.get('execute_ext_with').get(ext)
                file_vars, err = self.collect_variables_from_executable(env_file, program)

            if file_vars or err:
                print_as_title('Variables from {}'.format(env_file), '-', False, self.log)
                pretty_print_dict(file_vars, self.log)
                if err:
                    self.log(err)
                self.log()

        return file_vars


    def collect_variables_from_json_file(self, json_path, root_path):
        if json_path.startswith('.'):
            json_path = os.path.abspath(os.path.join(root_path, json_path))
        json_path = os.path.expandvars(json_path)

        with open(json_path, 'r') as json_file:
            json_data = json.load(json_file, object_pairs_hook=OrderedDict)

            env = OrderedDict()

            included_paths = json_data.get('include', tuple())
            for included_path in included_paths:
                inc_env = self.collect_variables_from_json_file(included_path, os.path.dirname(json_path))
                if inc_env[1]:
                    return OrderedDict(), inc_env[1]
                env = combine_dicts(inc_env[0], env)

            this_env = json_data.get('env')
            if this_env:
                env = combine_dicts(env, this_env)

            return env, None

    def collect_variables_from_command_line_file(self, command_file_path):
        pack_path = os.path.realpath(os.path.abspath(os.path.dirname(__file__)))
        print_env_py = os.path.normpath(os.path.join(pack_path, "utils/print_env.py"))

        if sublime.platform() == "windows":
            source_vars = os.path.join(pack_path, "utils/source_vars.bat")
        else:
            source_vars = os.path.join(pack_path, "utils/source_vars.sh")

        args = [source_vars, command_file_path, sys.executable, print_env_py]
        return self.__collect_variables_from_subprocess(command_file_path, args)


    def collect_variables_from_executable(self, executable_file_path, program):
        if program:
            args = [program, executable_file_path]
        else:
            args = [executable_file_path]

        return self.__collect_variables_from_subprocess(executable_file_path, args)


    def __collect_variables_from_subprocess(self, file_path, popen_args):
        popen_kwds = {'stdout': subprocess.PIPE,
                      'stderr': subprocess.PIPE,
                      'shell': False,
                      'universal_newlines': True}

        if(sublime.platform() == "windows"):
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            popen_kwds['startupinfo'] = startupinfo
            ingnore_envs = ('PROMPT', )
        else:
            ingnore_envs = ('__PYVENV_LAUNCHER__', 'LC_CTYPE', 'PWD', 'SHLVL', '_')

        with subprocess.Popen(args=popen_args, **popen_kwds) as p:
            stdout, stderr = p.communicate()

            if p.returncode == 0 and not stderr:
                try:
                    new_env = json.loads(stdout)

                    result_env = OrderedDict()
                    for key, value in new_env.items():
                        if key in ingnore_envs:
                            continue
                        elif key not in DEFAULT_ENV:
                            result_env[key] = value
                        elif value != DEFAULT_ENV[key]:
                            result_env[key] = value

                    return result_env, None

                except Exception as e:
                    if isinstance(e, ValueError):
                        if str(e) == "No JSON object could be decoded":
                            errorMsg = '__ERROR:__ {} doesn\'t return a valid Json string'.format(file_path)
                            return OrderedDict(), errorMsg
                    errorMsg = '__ERROR:__ ' + traceback.format_exc()
                    return OrderedDict(), errorMsg

            elif stderr:
                errorMsg = '__ERROR:__ ' + stderr
                return OrderedDict(), errorMsg
            else:
                errorMsg = '__WARNING:__ no environment returned'
                return OrderedDict(), errorMsg


class UpdateProjectDataCommand(sublime_plugin.WindowCommand):
    def run(self):
        data  = self.window.project_data()
        for folder in data['folders']:
            if "path_template" in folder:
                template = folder['path_template']
                resolved = os.path.expandvars(template)
                folder['path'] = resolved
        self.window.set_project_data(data)

    def is_enabled(self):
        data = self.window.project_data()
        if data:
            for folder in data['folders']:
                if "path_template" in folder:
                    return True
        return False

    def is_visible(self):
        return self.is_enabled()


class ClearProjectEnvironmentLogCommand(sublime_plugin.WindowCommand):
    def __init__(self, window):
        super().__init__(window)
        self.log = ProjectEnvironmentLog(window)

    def run(self):
        self.log.clear()

    def is_enabled(self):
        if get_project_env_settings():
            return True
        else:
            return False


class UpdateProjectEnvironmentSettingsCommand(sublime_plugin.WindowCommand):
    def is_visible(self):
        data = self.window.project_data()
        if not data:
            return False

        settings_data = data.get('settings')
        if not settings_data:
            return False

        setts = ['print_output', 'set_sublime_variables',
                 'sublime_variables_capitalized', 'sublime_variables_prefix',
                 'env', 'env_file']

        for sett in setts:
            if sett in settings_data:
                return True

        return False

    def is_enabled(self):
        return self.is_visible()


    def run(self):
        data = self.window.project_data()
        settings_data = data.get('settings')
        if not settings_data:
            return

        proj_env = OrderedDict()

        setts = ['print_output', 'set_sublime_variables', 'sublime_variables_capitalized', 'sublime_variables_prefix']
        for sett in setts:
            if sett in settings_data:
                proj_env[sett] = copy.deepcopy(settings_data[sett])

        old_oss = ('Windows', 'Darwin', 'Linux')
        new_oss = ('windows', 'osx', 'linux')

        for i, old_os in enumerate(old_oss):
            new_os = new_oss[i]
            if "env" in settings_data:
                if old_os in settings_data['env']:
                    proj_env[new_os] = OrderedDict()
                    proj_env[new_os]['env'] = copy.deepcopy(settings_data['env'][old_os])

            if "env_file" in settings_data:
                if old_os in settings_data['env_file']:
                    if new_os not in proj_env:
                        proj_env[new_os] = OrderedDict()
                    proj_env[new_os]['env_file'] = copy.deepcopy(settings_data['env_file'][old_os])

        for sett in setts + ['env', 'env_file']:
            if sett in data['settings']:
                del data['settings'][sett]
        data['settings']['project_environment'] = proj_env

        self.window.set_project_data(data)